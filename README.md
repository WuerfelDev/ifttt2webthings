# IFTTT2WebThings

Change the property of things in a Mozilla WebThings Gateway by an IFTTT trigger


<br>Before using this you should set up a new user account for these scripts in your Gateway!
<br>Then edit `inc/config.inc.php`: Enter your url/email/password and set a `secret`. It should be a randomly generated string.
<br>Your IFTTT applet could look like this:<br>
![](ifttt.jpg)


Useful:
- https://github.com/mozilla-iot/curl-examples
- https://iot.mozilla.org/wot