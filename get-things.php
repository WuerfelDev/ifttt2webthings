<?php

require "./inc/login.inc.php";

$ch = curl_init();
$options = array(CURLOPT_URL => $url.'/things/',
                 CURLOPT_HTTPHEADER => array('Authorization:Bearer '.$jwt, 'Content-Type: application/json', 'Accept: application/json'),
                 CURLOPT_RETURNTRANSFER => true
                );
curl_setopt_array($ch, $options);
$out = curl_exec($ch);
curl_close($ch);
$out = json_decode($out,true);
var_dump($out);
