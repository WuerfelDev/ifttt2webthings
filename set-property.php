<?php

// s for static secret
// t for thing
// p for property
// v for value

if(isset($_POST['s'])&&$_POST['s']==$secret&&isset($_POST['t'])&&isset($_POST['p'])&&isset($_POST['v'])){
require "./inc/login.inc.php";
$ch = curl_init();
$options = array(CURLOPT_URL => $url.'/things/'.$_POST['t'].'/properties/'.$_POST['p'],
                     CURLOPT_HTTPHEADER => array('Authorization:Bearer '.$jwt, 'Content-Type: application/json', 'Accept: application/json'),
                     CURLOPT_CUSTOMREQUEST => 'PUT',
                     CURLOPT_POSTFIELDS => '{"'.$_POST['p'].'":'.$_POST['v'].'}'
                    );
    curl_setopt_array($ch, $options);
    curl_exec($ch);
    curl_close($ch);
}else{
    http_response_code(403);
    die("Access forbidden.");
}
